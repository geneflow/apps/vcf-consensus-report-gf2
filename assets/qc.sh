#!/usr/bin/env bash
awk '$0 ~ ">" {if (NR > 1) {print c "\t" d;} c=0;d=0;printf substr($0,2,200) "\t"; } $0 !~ ">" {c+=length($0);d+=gsub(/N/, "");d+=gsub(/n/, "")} END { print c "\t" d; }' $1
